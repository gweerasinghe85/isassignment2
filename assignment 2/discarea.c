#include<stdio.h>
// This program will calculate the area of a disc

int main()
{
	float radius,area;
	const double PI=3.14;
	printf("Enter the radius of the disc:\n");
	scanf("%f",&radius);
	area=PI*radius*radius;
	printf("The area is: %.2f * %.2f * %.2f = %.4f\n",PI,radius,radius,area);
	return 0;

}
