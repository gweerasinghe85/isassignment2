#include<stdio.h>
// This program will multiply two floating points

int main()
{
	float x,y,z;
	printf("Enter the first number for the calculation\n");
	scanf("%f",&x);
	printf("Enter the second number for the calculation\n");
	scanf("%f",&y);
	z=x*y;
	printf("The answer is: %.2f * %.2f = %.4f\n",x,y,z);
	return 0;
}
