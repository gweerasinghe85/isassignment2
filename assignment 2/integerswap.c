#include<stdio.h>
// This is a program to swap two integers
int main()
{
	int x,y,z;
	printf("Enter the fist number: ");
	scanf("%d",&x);
	printf("Enter the second number: ");
	scanf("%d",&y);
	printf("\nBefore swapping the numbers, first number = %d second number = %d\n",x,y);
	z=x+y;
	x=z-x;
	y=z-y;
	printf("After swapping the numbers, first number = %d second number = %d\n",x,y);
	return 0;

}
